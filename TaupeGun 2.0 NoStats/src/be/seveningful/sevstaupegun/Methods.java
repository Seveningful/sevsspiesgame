package be.seveningful.sevstaupegun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

public class Methods {
	public static void broadcastTime(int seconds) {
		Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.GREEN + "TaupeGun" + ChatColor.GRAY + "] " +
				ChatColor.WHITE + seconds
				+ " seconds avant le d�but de la partie !");
	}
	
	public static void setXpTime(int seconds ){
		for(Player p : Bukkit.getOnlinePlayers()){
			p.setLevel(seconds);
		}
	}

	public static void fillTeams() {
		
		List<UUID> players = new ArrayList<UUID>();
		List<Team> teamstoshuffle = new ArrayList<Team>();
		for (Player noteam : Bukkit.getOnlinePlayers()) {

			if (Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(noteam) == null) {
				players.add(noteam.getUniqueId());

			}

		}
		for (Team teams : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
			if (!teams.getName().equals("Taupes")) {
				teamstoshuffle.add(teams);
			}
		}
		
		Collections.shuffle(teamstoshuffle);
		Collections.shuffle(players);
		
		for (UUID noteam : players) {
			
			for (Team teams : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {

					if (teams.getPlayers().size() < Main.instance.getConfiguration().getPlayersPerTeam()) {
						
						Player p = Bukkit.getPlayer(noteam);
						teams.addEntry(p.getName());
						p.setDisplayName(Bukkit.getScoreboardManager().getMainScoreboard().getEntryTeam(p.getName()).getPrefix()
										+ p.getName() + Bukkit.getScoreboardManager().getMainScoreboard().getEntryTeam(p.getName()).getSuffix());
						Bukkit.getPlayer(noteam)
								.sendMessage(
										ChatColor.GREEN
												+ "Vous avez rejoint une �quipe al�atoire !");
						
						p.setDisplayName(Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getPrefix()
								+ p.getName() + Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getSuffix());
						p.setPlayerListName(teams.getPrefix() + p.getName() + ChatColor.RESET);
					break;
					}

				}
			}
		
		
	}
}
