package be.seveningful.sevstaupegun.managers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import be.seveningful.sevstaupegun.Main;

public class Team {

	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public org.bukkit.scoreboard.Team getTeam() {
		return Bukkit.getScoreboardManager().getMainScoreboard().getTeam(name);
	}
	public Location getLoc() {
		return loc;
	}
	public ChatColor getChatcolor() {
		return chatcolor;
	}
	public DyeColor getDyecolor() {
		return dyecolor;
	}
	int id;
	
	String name;
	
	org.bukkit.scoreboard.Team team;
	
	Location loc;
	List<Taupe> taupes = new ArrayList<Taupe>();
	ChatColor chatcolor;
	DyeColor dyecolor;
	
	
	public Team(int id, String name, ChatColor chatcolor, DyeColor dyecolor, String suffix) {
		this.id = id;
		this.name = name;
		this.chatcolor = chatcolor;
		this.dyecolor = dyecolor;
		Scoreboard s = Bukkit.getScoreboardManager().getMainScoreboard();
		team = s.registerNewTeam(name);
		
		team.setPrefix(chatcolor.toString());
		team.setSuffix(suffix + ChatColor.WHITE.toString());
		team.setDisplayName(team.getPrefix() + team.getDisplayName()
				+ team.getSuffix());
		
		while (loc == null) {
			
			loc = SpawnManager.getRandomLocation(getTeam());
			
		}
	
	}
	
	public Team(int id, String name, ChatColor chatcolor, DyeColor dyecolor) {
		this.id = id;
		this.name = name;
		this.chatcolor = chatcolor;
		this.dyecolor = dyecolor;
		Scoreboard s = Bukkit.getScoreboardManager().getMainScoreboard();
		team = s.registerNewTeam(name);
		
		team.setPrefix(chatcolor.toString());
		team.setSuffix(ChatColor.WHITE.toString());
		team.setDisplayName(team.getPrefix() + team.getDisplayName()
				+ team.getSuffix());
		
		
			
			loc = SpawnManager.getRandomLocation(getTeam());
			
		
	}
	@SuppressWarnings("deprecation")
	public Taupe setTaupe(int id, int teamID) {
		
		org.bukkit.scoreboard.Team team  = getTeam();
		
		List<UUID> players = new ArrayList<UUID>();
		for (OfflinePlayer player2 : team.getPlayers()) {
			
			players.add(player2.getUniqueId());

		}
		
		
		
		Collections.shuffle(players);
		Random random = new Random();
		int psize = random.nextInt(players.size());
		UUID p = players.get(psize);
		Player taupe = Bukkit.getPlayer(p);
		if( ! taupe.isOnline()) return setTaupe(id, teamID);
		if(taupes.contains(TaupeManager.getTaupeByUUID(p))) return setTaupe(id, teamID);
		Taupe taupeplayer = new Taupe(id,p, this, false, false, teamID);
		this.taupes.add(taupeplayer);
		if(Main.instance.getConfiguration().getTaupeMessage().equalsIgnoreCase("DEFAULT")) {
			taupe.sendMessage(ChatColor.RED
					+ "-------Annonce IMPORTANTE------");
			taupe.sendMessage(ChatColor.GOLD
					+ "Vous �tes la taupe de votre �quipe !");
			taupe.sendMessage(ChatColor.GOLD
					+ "Pour parler avec les autres taupes �xecutez la commande /t <message>");
			taupe.sendMessage(ChatColor.GOLD
					+ "Si vous voulez d�voiler votre vraie identit� ex�cutez la commande /reveal");
			taupe.sendMessage(ChatColor.GOLD + "Votre but : "
					+ ChatColor.DARK_RED
					+ "Tuer les membres de votre \"�quipe\"");
			taupe.sendMessage(ChatColor.RED
					+ "-------------------------------");
		}else{
			taupe.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.instance.getConfiguration().getTaupeMessage()));
		}
		
		System.out.println("[TaupeGun] La taupe de l'�quipe " + getName() + " est " + Bukkit.getPlayer(taupeplayer.getUuid()).getName() + " [" + teamID + "]" );
		
		
		return taupeplayer;
		
	}
	
	public Taupe getTaupe(int TeamId) {
		for(Taupe t : taupes) {
			if(t.getTTeamID() == TeamId) return t;
		}
		return null;
		
	}
	
	public void unregister() {
		Main.instance.getTeamRegister().unregisterTeam(this);
	}
	

	
	
}
