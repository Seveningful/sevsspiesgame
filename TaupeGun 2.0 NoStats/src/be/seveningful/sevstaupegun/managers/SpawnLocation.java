package be.seveningful.sevstaupegun.managers;

import org.bukkit.Location;
import org.bukkit.scoreboard.Team;

public class SpawnLocation {

	
	public Location getLoc() {
		return loc;
	}

	public Team getTeam() {
		return team;
	}

	Location loc;
	
	Team team;

	public SpawnLocation(Location loc, Team team) {
		super();
		this.loc = loc;
		this.team = team;
	}
	
	
	
}
