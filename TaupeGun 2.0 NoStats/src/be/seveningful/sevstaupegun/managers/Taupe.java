package be.seveningful.sevstaupegun.managers;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Scoreboard;

import be.seveningful.sevstaupegun.Main;

public class Taupe {

	public boolean isRevealed() {
		return revealed;
	}
	public void setRevealed(boolean revealed) {
		this.revealed = revealed;
	}
	public boolean isDead() {
		return dead;
	}
	public boolean isSuperRevealed() {
		return srevealed;
	}
	public void setDead(boolean dead) {
		this.dead = dead;
	}
	public UUID getUuid() {
		return uuid;
	}
	public Team getTeam() {
		return team;
	}
	public boolean isSuper() {
		return supertaupe;
	}

	public int getTTeamID() {
		return taupeteamID;
	}
	boolean supertaupe = false;
	UUID uuid;
	Team team;
	int id;
	int taupeteamID;
	boolean revealed;
	boolean srevealed;
	boolean dead;

	public Taupe(int id,UUID uuid, Team team, boolean revealed, boolean dead, int taupeteamID) {
		this.id = id;
		this.uuid = uuid;
		this.team = team;
		this.revealed = revealed;
		this.dead = dead;
		this.taupeteamID = taupeteamID;
	}
	@SuppressWarnings("deprecation")
	public boolean reveal(boolean b) {
		if(this.revealed) return false;

		OfflinePlayer p  = Bukkit.getOfflinePlayer(uuid);
		if( !Main.instance.getGame().getAlivePlayers().contains(p.getUniqueId()))  {
			this.revealed = true;
			return false;
		}
		Bukkit.broadcastMessage(ChatColor.RED + p.getName() + " s'est r�v�l� �tre une taupe !");

		for(Player online : Bukkit.getOnlinePlayers()){
			online.playSound(online.getLocation(),
					Sound.GHAST_SCREAM, 10.0F, -10.0F);
		}

		if(b) p.getPlayer().getWorld().dropItem(p.getPlayer().getLocation(), new ItemStack(Material.GOLDEN_APPLE));
		Scoreboard s =  Bukkit.getScoreboardManager().getMainScoreboard();



		org.bukkit.scoreboard.Team ancientteam = Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p);
		if(ancientteam.getSize() == 1) {
			Main.instance.getTeamRegister().getTeamByName(ancientteam.getName()).unregister();
		}
		this.revealed = true;

		if (s.getTeam("taupes" + taupeteamID ) == null) {
			Team tteams = (new Team(Main.instance.getTeamRegister().teams.size() + 1, "taupes" + taupeteamID , ChatColor.RED, DyeColor.RED, " [" + taupeteamID + "]"));
			TeamsRegister.registerTeam(tteams);
			tteams.getTeam().addPlayer(p);
		} else {

			org.bukkit.scoreboard.Team team = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("taupes" + taupeteamID );
			team.addPlayer(p);
		}
		if(p.isOnline()) {
			Player connected = p.getPlayer();
			connected.setDisplayName(Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getPrefix()
					+ p.getName() + Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getSuffix());
			connected.setPlayerListName(Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getPrefix() + p.getName() + Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getSuffix());
		}
		return true;

	}
	public int getId() {
		return id;
	}
	public boolean setSuper() {
		Player p = Bukkit.getPlayer(getUuid());

		if(!p.isOnline()) return false;
		if(isDead()) return false;

		if(Main.instance.getConfiguration().getSuperTaupeMessage().equalsIgnoreCase("DEFAULT"))
			p.sendMessage(ChatColor.RED + "VOUS ETES UNE SUPER TAUPE !");
		else
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.instance.getConfiguration().getSuperTaupeMessage()));
		p.playSound(p.getLocation(), Sound.WITHER_SHOOT, 10, -10);
		TitleManager.sendActionBar(p, ChatColor.RED + "VOUS ETES UNE SUPER TAUPE !");


		supertaupe = true;


		return true;


	}
	public boolean superreveal(boolean b) {
		if(this.isSuperRevealed()) return false;

		OfflinePlayer p  = Bukkit.getOfflinePlayer(uuid);
		if( !Main.instance.getGame().getAlivePlayers().contains(p.getUniqueId()))  {
			this.srevealed = true;
			return false;
		}
		Bukkit.broadcastMessage(ChatColor.DARK_RED + p.getName() + " s'est r�v�l� �tre une SUPERtaupe !");

		for(Player online : Bukkit.getOnlinePlayers()){
			online.playSound(online.getLocation(),
					Sound.GHAST_SCREAM, 10.0F, -10.0F);
		}

		if(b) p.getPlayer().getWorld().dropItem(p.getPlayer().getLocation(), new ItemStack(Material.GOLDEN_APPLE));
		Scoreboard s =  Bukkit.getScoreboardManager().getMainScoreboard();



		org.bukkit.scoreboard.Team ancientteam = Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p);
		if(ancientteam.getSize() == 1) {
			if(Main.instance.getTeamRegister().getTeamByName(ancientteam.getName()) != null)
				Main.instance.getTeamRegister().getTeamByName(ancientteam.getName()).unregister();
		}
		this.revealed = true;

		if (s.getTeam("supertaupes" ) == null) {
			Team tteams = (new Team(Main.instance.getTeamRegister().teams.size() + 1, "supertaupes" , ChatColor.DARK_RED, DyeColor.RED));
			TeamsRegister.registerTeam(tteams);
			tteams.getTeam().addPlayer(p);
		} else {

			org.bukkit.scoreboard.Team team = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("supertaupes" );
			team.addPlayer(p);
		}
		if(p.isOnline()) {
			Player connected = p.getPlayer();
			connected.setDisplayName(Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getPrefix()
					+ p.getName() + Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getSuffix());
			connected.setPlayerListName(Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getPrefix() + p.getName() + Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getSuffix());
		}
		this.srevealed = true;
		return true;

	}



}
