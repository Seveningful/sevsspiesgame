package be.seveningful.sevstaupegun.managers;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.OfflinePlayer;

import be.seveningful.configmenu.OptionsMenu;
import be.seveningful.sevstaupegun.Main;


public class TeamsRegister {

	static ArrayList<Team> teams = new ArrayList<Team>();
	
	
	
	public TeamsRegister() {
		for(org.bukkit.scoreboard.Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
			team.unregister();
		
		}
		teams.clear();
		
		if(OptionsMenu.getOption("options.teams").getCurrentIntValue() >= 2) { 
		teams.add(new Team(1, "Cyan", ChatColor.DARK_AQUA, DyeColor.CYAN));
		teams.add(new Team(2, "Rose", ChatColor.LIGHT_PURPLE, DyeColor.PINK));
		}
		if(OptionsMenu.getOption("options.teams").getCurrentIntValue() >= 3)
		teams.add(new Team(3, "Orange", ChatColor.GOLD, DyeColor.ORANGE));
		if(OptionsMenu.getOption("options.teams").getCurrentIntValue() >= 4)
		teams.add(new Team(4, "Grise", ChatColor.GRAY, DyeColor.GRAY));
		if(OptionsMenu.getOption("options.teams").getCurrentIntValue() >= 5)
		teams.add(new Team(5, "Verte", ChatColor.GREEN, DyeColor.LIME));
		if(OptionsMenu.getOption("options.teams").getCurrentIntValue() >= 6)
		teams.add(new Team(6, "Jaune", ChatColor.YELLOW, DyeColor.YELLOW));
		if(OptionsMenu.getOption("options.teams").getCurrentIntValue() >= 7)
			teams.add(new Team(7, "Vert fonc�", ChatColor.DARK_GREEN, DyeColor.GREEN));
		if(OptionsMenu.getOption("options.teams").getCurrentIntValue() >= 8) {
			teams.add(new Team(8, "Bleu fonc�", ChatColor.DARK_BLUE, DyeColor.BLUE));
		}
	}
	
	public ArrayList<Team> getregisteredTeams() {
		return teams;
	}
	
	public void unregisterTeam(Team team) {
		//Bukkit.broadcastMessage("L'�quipe " +t.getPrefix() + t.getName() + t.getSuffix() + " a �t� �limin�e");
		teams.remove(team);
		if(team.getTeam() != null)
		team.getTeam().unregister();
	}

	public Team getTeamByName(String string) {
		for(Team team : teams) {
			if(team.getName().equalsIgnoreCase(string)) return team;
		}
		return null;
	}
	
	public Team getTeamOfPlayer(OfflinePlayer p) {
		for(Team team : teams) {
			for(OfflinePlayer player : team.getTeam().getPlayers()) {
				if(player == p) return team;
			}
		}
		return null;
	}
	
	public Team getTeamByID(int i) {
		for(Team team : teams) {
			if(team.getId() == i) return team;
		}
		return null;
		
	}
	public static void registerTeam(Team t) {
		teams.add(t);
	}
	public void update() {
		for(org.bukkit.scoreboard.Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
			team.unregister();
		}
		
		teams.clear();
		if(Main.instance.getConfiguration().getTeams() >= 2) { 
		teams.add(new Team(1, "Cyan", ChatColor.DARK_AQUA, DyeColor.CYAN));
		teams.add(new Team(2, "Rose", ChatColor.LIGHT_PURPLE, DyeColor.PINK));
		}
		if(Main.instance.getConfiguration().getTeams() >= 3)
		teams.add(new Team(3, "Orange", ChatColor.GOLD, DyeColor.ORANGE));
		if(Main.instance.getConfiguration().getTeams() >= 4)
		teams.add(new Team(4, "Grise", ChatColor.GRAY, DyeColor.GRAY));
		if(Main.instance.getConfiguration().getTeams() >= 5)
		teams.add(new Team(5, "Verte", ChatColor.GREEN, DyeColor.GREEN));
		if(Main.instance.getConfiguration().getTeams() >= 6)
		teams.add(new Team(6, "Jaune", ChatColor.YELLOW, DyeColor.YELLOW));
		
	}
}
