package be.seveningful.sevstaupegun.managers;

public enum ScoreboardVariables {

	BORDER(),PLAYERS(),TEAMS(), TIME(), TAUPES_ALIVE();
}
