package be.seveningful.sevstaupegun.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import be.seveningful.sevstaupegun.Main;

public class SpawnManager {

	static List<SpawnLocation> spawns = new ArrayList<SpawnLocation>();
	
	
	
	public static  Location getRandomLocation(org.bukkit.scoreboard.Team team) {
		
		 	double size = Bukkit.getWorld("world").getWorldBorder().getSize() / 2;
		 
		    Location loc = new Location(Main.instance.getConfiguration().getGameWorld(), nextInt((int)-size, (int)size), 255, nextInt((int)-size, (int) size));
		    while (!isCorrectPosition(loc)) {
				loc = getRandomLocation(team);
				
			} 
		    System.out.println("Registered spawn at X:" + loc.getX() + " Y:"+ loc.getY() + " Z:" + loc.getZ());
		    return loc;
		    
		    

		}
		 
		public static int nextInt(int min, int max) {
		    Random rnd = new Random(); // the random thingy
		    return rnd.nextInt(Math.abs(max - min) + 1) + min; // get a new random number inside the limits
		}
		
		private static boolean isCorrectPosition(Location loc){
			
			for(SpawnLocation locs : spawns) {
				
				if(locs.getLoc().distance(loc) < Main.instance.getConfiguration().getMinimalSpawnDistance() && loc.distance(locs.getLoc()) < Main.instance.getConfiguration().getMinimalSpawnDistance()) {
					return false;
				
			}
			}
			return true;
		
		}
	
}
