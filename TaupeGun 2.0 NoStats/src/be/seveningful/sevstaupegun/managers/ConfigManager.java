package be.seveningful.sevstaupegun.managers;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.FileConfiguration;

import be.seveningful.configmenu.OptionsMenu;
import be.seveningful.sevstaupegun.Main;

public class ConfigManager {
	FileConfiguration config ;

	public void registerDefaults(Main main) {

		
		config = main.getConfig();

		/**
		 * WorldBorder
		 */
		main.getConfig().addDefault("worldborder.size", 1500);
		main.getConfig().addDefault("worldborder.retract-after-minutes", 130);
		main.getConfig().addDefault("worldborder.final-size", 100);

		/**
		 * Potions
		 */
		main.getConfig().addDefault("potions.allow-glowstone", false);
		main.getConfig().addDefault("potions.allow-regeneration", false);
		main.getConfig().addDefault("potions.allow-strength", false);
	

		/**
		 * Commands
		 */
		main.getConfig().addDefault("commands.me-command", false);
		main.getConfig().addDefault("commands.tell-command", false);

		/**
		 * Game Options
		 */
		main.getConfig().addDefault("options.stop-time-cycle-after-minutes", 30);

		
		main.getConfig().addDefault("options.no-damage-time", 20);
		
		main.getConfig().addDefault("options.time-cycle", true);
		
		main.getConfig().addDefault("options.min-players", Integer.valueOf(20));
		
		main.getConfig().addDefault("options.pvp-time", Integer.valueOf(20));
		
		main.getConfig().addDefault("options.cooldown", false);
	
		main.getConfig().addDefault("options.has-supertaupes", true);
		
		main.getConfig().addDefault("options.players-per-team", 4);

		main.getConfig().addDefault("options.set-taupes-after-minutes", 30);

		main.getConfig().addDefault("options.force-reveal-after-minutes", 55);
		
		main.getConfig().addDefault("options.game-name", "Taupe Gun");
		
		main.getConfig().addDefault("options.teams", 5);
		
		main.getConfig().addDefault("options.minimal-spawn-distance", 500);

		main.getConfig().addDefault("options.taupes-per-team", 1);
	
		main.getConfig().addDefault("worldborder.retract-time", 15);

		main.getConfig().addDefault("options.time-to-supertaupes", 40);

		main.getConfig().addDefault("options.time-to-reveal-supertaupes", 70);
		
		/*
		 * MEssages
		 */
		
		main.getConfig().addDefault("messages.taupe", "DEFAULT");
		main.getConfig().addDefault("messages.super-taupe", "DEFAULT");
		
		/*
		 * MOTD
		 */
		
		main.getConfig().addDefault("motd.prepare", "&6Phase de pr�paration");
		
		main.getConfig().addDefault("motd.starting", "&aDemarrage du TaupeGun");
		
		main.getConfig().addDefault("motd.running", "En cours");
		
		main.getConfig().addDefault("motd.no-pvp", "&ePhase pre-PVP");
		
		main.getConfig().addDefault("motd.finished", "&4Partie finie !");
		
		
		
		/**
		 *Lobby 
		 */
		main.getConfig().addDefault("lobby.world", "lobby");
		main.getConfig().addDefault("lobby.X", Integer.valueOf(0));
		main.getConfig().addDefault("lobby.Y", Integer.valueOf(100));
		main.getConfig().addDefault("lobby.Z", Integer.valueOf(0));
		
		OptionsMenu.registerOption("GlowStone","potions.allow-glowstone");
		OptionsMenu.registerOption( "Potion de force","potions.allow-strength");
		OptionsMenu.registerOption("Potion de reg�n�ration","potions.allow-regeneration");
		OptionsMenu.registerOption( "Commande /me","commands.me-command");
		OptionsMenu.registerOption("Commande /tell","commands.tell-command");
		OptionsMenu.registerOption( "Eternal-Day apr�s (minutes) > ","options.stop-time-cycle-after-minutes");
		OptionsMenu.registerOption( "Temps avant l'activation des d�g�ts (secondes) > ","options.no-damage-time");
		OptionsMenu.registerOption( "Cycle du temps","options.time-cycle");
		OptionsMenu.registerOption( "Nombre de joueurs minimal > ","options.min-players");
		OptionsMenu.registerOption( "Temps avant l'activation du pvp (minutes) > ","options.pvp-time");
		OptionsMenu.registerOption( "Cooldown pour d�but de partie","options.cooldown");
		OptionsMenu.registerOption( "SuperTaupes","options.has-supertaupes");
		OptionsMenu.registerOption( "Joueurs par �quipe > ","options.players-per-team");
		OptionsMenu.registerOption( "D�fintion des taupes apr�s (minutes) > ","options.set-taupes-after-minutes");
		OptionsMenu.registerOption( "Force-Reveal des taupes (minutes) > ","options.force-reveal-after-minutes");
		OptionsMenu.registerOption( "Nom du jeu > ","options.game-name");
		OptionsMenu.registerOption( "Nombre d'�quipes > ","options.teams");
		OptionsMenu.registerOption( "Taupes par �quipe > ","options.taupes-per-team");
		OptionsMenu.registerOption( "Distance minimale entre les spawns > ","options.minimal-spawn-distance");
		OptionsMenu.registerOption( "Temps de retraction des bordures (minutes) > ","worldborder.retract-time");
		OptionsMenu.registerOption( "Temps avant la d�finition des Supertaupes (minutes) > ","options.time-to-supertaupes");
		OptionsMenu.registerOption( "Force-Reveal des SuperTaupes (minutes) > ","options.time-to-reveal-supertaupes");
		OptionsMenu.registerOption( "MOTD (Phase de pr�paration) > ","motd.prepare");
		OptionsMenu.registerOption( "MOTD (Phase pr�-PVP) > ","motd.no-pvp");
		OptionsMenu.registerOption( "MOTD (D�but du TaupeRun) > ","motd.starting");
		OptionsMenu.registerOption( "MOTD (En cours) > ","motd.running");
		OptionsMenu.registerOption( "MOTD (Partie finie) > ","motd.finished");
		OptionsMenu.registerOption("Taille de la bordure >","worldborder.size");
		OptionsMenu.registerOption("Temps avant la r�traction des bordures (minutes) >","worldborder.retract-after-minutes");
		OptionsMenu.registerOption("Taille finale de la bordure >","worldborder.final-size");
	}
	
	
	public double getWBSize() {
		
		return config.getDouble("worldborder.size");
	}
	
	public double getCycleAfter() {
		
		return config.getDouble("options.stop-time-cycle-after-minutes");
	}
	
	public int getWBRetractAfterMinutes() {
		return config.getInt("worldborder.retract-after-minutes");
		
	}
	
	public double getWBFinalSize() {
		return config.getDouble("worldborder.final-size");
	}
	public int getRetractTime() {
		return config.getInt("worldborder.retract-time");
	}
	
	public boolean hasLevelTwoPotions() {
		return config.getBoolean("potions.allow-glowstone");
	}
	
	public boolean hasStrengthPotions() {
		return config.getBoolean("potions.allow-strength");
	}
	
	public boolean hasRegenerationPotions() {
		return config.getBoolean("potions.allow-regeneration");
	}
	
	public boolean hasMeCommand() {
		return config.getBoolean("commands.me-command");
	}
	
	public boolean hasTellCommand() {
		return config.getBoolean("commands.tell-command");
	}
	
	public int getNoDamageTime() {
		return config.getInt("options.no-damage-time");
	}
	
	public boolean hasTimeCycle() {
		return config.getBoolean("options.time-cycle");
	}
	
	public int getMinimumPlayers() {
		return config.getInt("options.min-players");
	}
	
	public int getPVPTime() {
		
		return config.getInt("options.pvp-time");
	}
	
	public boolean hasCooldown() {
		return config.getBoolean("options.cooldown");
	}
	
	public boolean hasSuperTaupes() {
		return config.getBoolean("options.has-supertaupes");
	}
	
	public int getPlayersPerTeam() {
		return config.getInt("options.players-per-team");
	}
	
	public int getTimeToSetTaupes() {
		return config.getInt("options.set-taupes-after-minutes");
	}
	
	public int getForceRevealMinutes() {
		return config.getInt("options.force-reveal-after-minutes");
	}
	
	public World getLobbyWorld() {
		return Bukkit.getWorld(config.getString("lobby.world"));
	}
	
	public String getLobbyWorldName() {
		return config.getString("lobby.world");
	}
	
	public Location getLobbyLocation() {
		return new Location(getLobbyWorld(), config.getInt("lobby.X"),  config.getInt("lobby.Y"),  config.getInt("lobby.Z"));
	}

	public String getTaupeMessage(){
		return config.getString("messages.taupe");
	}
	public String getSuperTaupeMessage(){
		return config.getString("messages.super-taupe");
	}

	public World getGameWorld() {
		return Bukkit.getWorld("world");
	}


	public String getGameName() {
		// TODO Auto-generated method stub
		return config.getString("options.game-name");
	}


	public int getTeams() {
		// TODO Auto-generated method stub
		return config.getInt("options.teams");
	}
	
	
	public void setIntValue(String path, int value){
		
		config.set(path, value);
	}
	public void setStringValue(String path, String value){
		
		config.set(path, value);
		
	}


	public double getMinimalSpawnDistance() {
		return config.getDouble("options.minimal-spawn-distance");
	}


	public int getNumberOfTaupesTeams() {
		return config.getInt("options.taupes-per-team");
	}

	public String getMOTD(String state){
	
		return config.getString("motd." + state);
		
	}


	public int getSuperTaupesTime() {
		return config.getInt("options.time-to-supertaupes");
	}


	public int getTimetoForceSuperReveal() {
		return config.getInt("options.time-to-reveal-supertaupes");
	}
}