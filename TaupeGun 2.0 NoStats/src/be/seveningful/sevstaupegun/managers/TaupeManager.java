package be.seveningful.sevstaupegun.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import be.seveningful.sevstaupegun.Main;

public class TaupeManager {


	static List<Taupe> taupes = new ArrayList<Taupe>();

	Main main;

	public TaupeManager(Main main) {

		this.main = main;

		setupTaupes();
		autoReveal();

	}

	private void autoReveal() {
		new BukkitRunnable() {
			@Override
			public void run() {
				for(Taupe taupe : taupes) {
					taupe.reveal(false);
				}


			}
		}.runTaskLater(main, 20*60*main.getConfiguration().getForceRevealMinutes());

	}

	public void setupTaupes() {

		new BukkitRunnable() {
			@Override
			public void run() {
				int id = 1;
				for(int x = 0 ; x < Main.instance.getConfiguration().getNumberOfTaupesTeams(); x++) {
					for(Team team : main.getTeamRegister().getregisteredTeams()) {
						taupes.add(team.setTaupe(id, x));
						id ++;
					}
				}


			}
		}.runTaskLater(main, 20*60*main.getConfiguration().getTimeToSetTaupes());

	}

	public static List<Taupe> getTaupes() {

		return taupes;

	}
	public static Taupe getTaupeByUUID(UUID id) {
		for(Taupe taupe : taupes) {
			if( taupe.uuid.toString().equalsIgnoreCase(id.toString())) return taupe;
		}
		return null;
	}
	public static Taupe getSuperTaupeByUUID(UUID id) {
		for(Taupe taupe : taupes) {
			if(taupe.isSuper())
				if( taupe.uuid.toString().equalsIgnoreCase(id.toString())) return taupe;
		}
		return null;
	}

	public static List<Taupe> getAliveTaupes(int id) {
		List<Taupe> taupesalive =new ArrayList<Taupe>();

		for(Taupe taupe : taupes) {
			if(!taupe.isDead()&& taupe.getTTeamID() == id && !taupe.isSuperRevealed()) {
				taupesalive.add(taupe);
			}
		}
		return taupesalive;
	}


	public static List<Player> getSuperTaupes() {
		List<Player> players = new ArrayList<Player>();

		for(Taupe t : getTaupes()) {
			if(Bukkit.getPlayer(t.getUuid()).isOnline())
				if(t.isSuper()) players.add(Bukkit.getPlayer(t.getUuid()));
		}
		return players;


	}

	public static List<Taupe> getAliveSuperTaupes() {
		List<Taupe> taupesalive =new ArrayList<Taupe>();
		for(Taupe taupe : taupes) {
			if(!taupe.isDead() && taupe.isSuper()) {
				taupesalive.add(taupe);
			}
		}
		return taupesalive;
	}

	public static List<Taupe> getAllAliveTaupes() {
		List<Taupe> taupesalive =new ArrayList<Taupe>();

		for(Taupe taupe : taupes) {
			if(!taupe.isDead() && !taupe.isSuperRevealed()) {
				taupesalive.add(taupe);
			}
		}
		return taupesalive;
	}
}
