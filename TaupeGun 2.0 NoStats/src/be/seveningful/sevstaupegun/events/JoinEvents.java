package be.seveningful.sevstaupegun.events;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import be.seveningful.sevstaupegun.GameState;
import be.seveningful.sevstaupegun.Main;
import be.seveningful.sevstaupegun.managers.TitleManager;

public class JoinEvents implements Listener {

	
	public static Map<UUID, Scoreboard> scoreboards = new HashMap<UUID, Scoreboard>();

	@EventHandler
	public void Join(PlayerJoinEvent e) {
		Player p = e.getPlayer();




		if(Main.instance.getGame().getGameState() == GameState.STARTING) {
			p.getInventory().clear();
			p.getInventory().setArmorContents(new ItemStack[]{});
			Main.instance.getScoreboard().setUpDefaultScoreBoard(p);
			p.teleport(Main.instance.getConfiguration().getLobbyLocation());
			p.getInventory().setItem(0, new ItemStack(Material.BANNER, 1));
			p.setGameMode(GameMode.ADVENTURE);

			ItemMeta meta1 = p.getInventory().getItem(0).getItemMeta();
			meta1.setDisplayName(ChatColor.GOLD + "Choisir son �quipe");
			p.getInventory().getItem(0).setItemMeta(meta1);

			p.setFoodLevel(20);
			p.setHealth(20.0);
			TitleManager.sendTitle(ChatColor.WHITE + "SevsSpiesGame", p, 10, 20*3, 10);
			TitleManager.sendSubTitle(ChatColor.GREEN + "Plugin par " + ChatColor.WHITE + "Sevening" + ChatColor.GREEN + " & " + ChatColor.WHITE + "NathPsgMan", p, 10, 20*3, 10);
			e.setJoinMessage(ChatColor.BLUE + p.getName() + ChatColor.YELLOW
					+ " a rejoint la partie  " + ChatColor.GRAY + "("
					+ ChatColor.YELLOW + Bukkit.getOnlinePlayers().size() + "/"
					+ Bukkit.getMaxPlayers() + ChatColor.GRAY + ")");

		} else if (Main.instance.getGame().getGameState() != GameState.STARTING && !Main.instance.getGame().getAlivePlayers().contains(p.getUniqueId()) ) {
			if(p.getScoreboard().getObjective(DisplaySlot.SIDEBAR) != null) {
				p.getScoreboard().getObjective(DisplaySlot.SIDEBAR).unregister();
			}
			if(p.getScoreboard().getObjective(DisplaySlot.PLAYER_LIST) == null) {

				Objective obj = p.getScoreboard().registerNewObjective("health", "health");
				obj.setDisplaySlot(DisplaySlot.PLAYER_LIST);
			}

			e.setJoinMessage(ChatColor.GRAY + ChatColor.ITALIC.toString()
					+ p.getName() + " a rejoint la partie  ");
			p.teleport(new Location(Main.instance.getConfiguration().getGameWorld(), 0.0D, 0.0D, 0.0D));
			p.setGameMode(GameMode.SPECTATOR);
		} else  {
			if(p.getScoreboard().getObjective(DisplaySlot.SIDEBAR) != null) {
				p.getScoreboard().getObjective(DisplaySlot.SIDEBAR).unregister();
				p.setDisplayName(Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getPrefix()
						+ p.getName() + Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getSuffix());
				p.setPlayerListName(Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getPrefix() + p.getName() + ChatColor.RESET);
			}

		}
		
			e.getPlayer().setScoreboard(Main.instance.getScoreboard().getScoreboard(e.getPlayer().getUniqueId()));
		

		/*
		 * Scoreboard
		 */

	}
}
