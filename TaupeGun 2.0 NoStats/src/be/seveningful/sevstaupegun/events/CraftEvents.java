package be.seveningful.sevstaupegun.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftShapedRecipe;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import be.seveningful.sevstaupegun.Main;

public class CraftEvents implements Listener {
	
	/**
	 * Brew Event
	 * Cancels : 
	 * Level 2 Potions
	 * Regenration Potions
	 * Strength Potions
	 */
	@EventHandler
	public void BrewCancel(BrewEvent e) {
		BrewerInventory bi = e.getContents();
		if (bi.getIngredient().getType().equals(Material.GLOWSTONE_DUST)
				&& !Main.instance.getConfiguration().hasLevelTwoPotions()) {
			for (HumanEntity player : bi.getViewers()) {
				player.sendMessage(ChatColor.RED
						+ "Les potions de niveau 2 sont interdites !");
			}
			e.setCancelled(true);

		} else if (bi.getIngredient().getType().equals(Material.BLAZE_POWDER)
				&& !Main.instance.getConfiguration().hasStrengthPotions()) {
			e.setCancelled(true);
			for (HumanEntity player : bi.getViewers()) {
				player.sendMessage(ChatColor.RED
						+ "Les potions de force sont interdites !");
			}
		} else if (bi.getIngredient().getType().equals(Material.GHAST_TEAR)
				&& !Main.instance.getConfiguration().hasRegenerationPotions()) {
			e.setCancelled(true);
			for (HumanEntity player : bi.getViewers()) {
				player.sendMessage(ChatColor.RED
						+ "Les potions de r�g�n�ration sont interdites !");
			}
		}
	}
	
	@EventHandler
	public void CancelCraft(PrepareItemCraftEvent e) {
		Recipe recipe = e.getRecipe();
		ItemStack NotchApple = new ItemStack(Material.GOLDEN_APPLE, 1,
				(short) 1);
		CraftShapedRecipe craft = new CraftShapedRecipe(new ItemStack(
				Material.SPECKLED_MELON));
		craft.shape(new String[] { "abc", "def", "ghi" });
		craft.setIngredient('a', Material.GOLD_NUGGET);
		craft.setIngredient('b', Material.GOLD_NUGGET);
		craft.setIngredient('c', Material.GOLD_NUGGET);
		craft.setIngredient('d', Material.GOLD_NUGGET);
		craft.setIngredient('f', Material.GOLD_NUGGET);
		craft.setIngredient('g', Material.GOLD_NUGGET);
		craft.setIngredient('h', Material.GOLD_NUGGET);
		craft.setIngredient('i', Material.GOLD_NUGGET);

		craft.setIngredient('e', Material.MELON);
		Bukkit.addRecipe(craft);

		if (recipe instanceof CraftShapedRecipe) {
			if (((CraftShapedRecipe) e.getRecipe()).getIngredientMap().equals(
					craft.getIngredientMap())) {
				e.getInventory().setResult(new ItemStack(Material.AIR));
				for (HumanEntity p : e.getViewers()) {
					p.sendMessage("�4Ce craft a �t� modifi� !");
				}
			} else if (recipe.getResult().equals(NotchApple)) {
				e.getInventory().setResult(new ItemStack(Material.AIR));
				for (HumanEntity p : e.getViewers()) {
					p.sendMessage("�4Ce craft a �t� d�sactiv� !");
				}
			}
		}
	}

	
	
	
	
}
