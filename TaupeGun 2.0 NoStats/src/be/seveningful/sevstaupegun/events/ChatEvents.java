package be.seveningful.sevstaupegun.events;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.scoreboard.Team;

import be.seveningful.sevstaupegun.Main;

public class ChatEvents implements Listener {

	
	  @EventHandler
	    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event)  {
	        if(event.getMessage().contains("/me") && !Main.instance.getConfiguration().hasMeCommand() ) {
	            event.setCancelled(true);
	        } else   if(event.getMessage().contains("/tell") && !Main.instance.getConfiguration().hasTellCommand() ) {
	            event.setCancelled(true);
	        }
	    }
	  
		@EventHandler
		public void ChatSpec(AsyncPlayerChatEvent e) {
			Player p = e.getPlayer();
			if (!Main.instance.getGame().getAlivePlayers().contains(p.getUniqueId())) {
				e.setCancelled(true);
				for (Player dead : getDeathPlayers()) {
					dead.sendMessage("<" + ChatColor.GRAY
							+ ChatColor.ITALIC.toString() + p.getName()
							+ ChatColor.RESET + "> " + e.getMessage());
				}
			} else {
				Team t = Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(e.getPlayer());
				e.setFormat("<" + t.getPrefix() + e.getPlayer().getName() + t.getSuffix() + ChatColor.WHITE + "> "+ e.getMessage());
			}
		}
		
		public ArrayList<Player> getDeathPlayers() {
			ArrayList<Player> deadList = new ArrayList<Player>();
			for (Player dead : Bukkit.getOnlinePlayers()) {
				if (!Main.instance.getGame().getAlivePlayers().contains(dead.getUniqueId())) {
					deadList.add(dead);
				}

			}
			return deadList;
		}
	
}
