package be.seveningful.sevstaupegun.events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import be.seveningful.configmenu.Anvil;
import be.seveningful.configmenu.ModificationInventory;
import be.seveningful.configmenu.OptionsMenu;
import be.seveningful.sevstaupegun.GameState;
import be.seveningful.sevstaupegun.Main;
import be.seveningful.sevstaupegun.WorldLoader;
import be.seveningful.sevstaupegun.WorldSetup;
import be.seveningful.sevstaupegun.managers.TeamsRegister;

public class ConfigManagement implements Listener{
	@EventHandler(priority = EventPriority.MONITOR)
	public void onInventoryClick(InventoryClickEvent e){
			HumanEntity ent = e.getWhoClicked();

			if(ent instanceof Player){
				Player player = (Player)ent;
				Inventory inv = e.getInventory();

				if(inv.getTitle().contains("Repair") && Main.instance.getGame().getGameState() == GameState.STARTING){
					e.setCancelled(true);
					InventoryView view = e.getView();
					int rawSlot = e.getRawSlot();

					if(rawSlot == view.convertSlot(rawSlot)){
						if(rawSlot == 2){
							ItemStack item = e.getCurrentItem();

							if(item != null){
								ItemMeta meta = item.getItemMeta();

								if(meta != null){
									if(meta.hasDisplayName()){
										String displayName = meta.getDisplayName();
										
										if(OptionsMenu.getOption(item.getItemMeta().getLore().get(0)).isInt())
											OptionsMenu.getOption(item.getItemMeta().getLore().get(0)).setCurrentIntvalue(Integer.parseInt(displayName));
										else
											OptionsMenu.getOption(item.getItemMeta().getLore().get(0)).setCurrentvalue(displayName);
										player.sendMessage(ChatColor.GREEN + "Option modifi�e");
										e.getInventory().clear();
										e.setCancelled(true);
										OptionsMenu.getInstance().update();
										e.getView().close();
										OptionsMenu.openTo(player);
										
										player.playSound(player.getLocation(), Sound.SUCCESSFUL_HIT, 10, 10);
									}
								}
							}
					}
				}
			}else if(inv.getTitle().contains("Menu des options")) {
				ItemStack item = e.getCurrentItem();
				if(item.getType() == Material.INK_SACK) {
					
					if (OptionsMenu.getOption(item.getItemMeta().getLore().get(0)).isBoolean) {
						OptionsMenu.getOption(item.getItemMeta().getLore().get(0)).setCurrentBooleanValue(!OptionsMenu.getOption(item.getItemMeta().getLore().get(0)).currentBooleanValue);
						OptionsMenu.getInstance().update();
						player.sendMessage(ChatColor.GREEN + "Option modifi�e");
						e.setCancelled(true);
						return;
					}
					e.getView().close();
					
					String option = item.getItemMeta().getLore().get(0);
					Anvil.openAnvilInventory((Player) e.getWhoClicked(), option);
					e.setCancelled(true);
					player.playSound(player.getLocation(), Sound.NOTE_PIANO, 10, 10);
					
				} else if ( item.getType() == Material.SLIME_BALL ) {
					Main.instance.saveConfig();
					Main.instance.reloadConfig();
					Main.instance.setupConfig();
					new WorldLoader(Main.instance, false);
					new WorldSetup(Main.instance);
					Main.instance.setTeamRegister(new TeamsRegister());
				
					e.setCancelled(true);
					e.getWhoClicked().sendMessage(ChatColor.GREEN + "Configuration recharg�e !");
					player.playSound(player.getLocation(), Sound.CAT_MEOW, 10, 10);
					OptionsMenu.getInstance().update();
				}
			} 
		}
	}
	
	
	
}
