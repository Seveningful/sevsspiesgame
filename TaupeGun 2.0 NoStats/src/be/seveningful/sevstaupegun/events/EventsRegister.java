package be.seveningful.sevstaupegun.events;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

import be.seveningful.sevstaupegun.Main;


public class EventsRegister {

	
	public EventsRegister(Main main) {

	PluginManager pm = Bukkit.getPluginManager();
	
	
	pm.registerEvents(new JoinEvents(), main);
	pm.registerEvents(new NormalUHCEvents(), main);
	pm.registerEvents(new CraftEvents(), main);
	
	pm.registerEvents(new ChatEvents(), main);
	pm.registerEvents(new ChoiceTeam(), main);
	pm.registerEvents(new FunnyEventsRegister(), main);
	pm.registerEvents(new ConfigManagement(), main);
	
	
	}
	
	
}
