package be.seveningful.sevstaupegun.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CountDownFinish extends Event {
	private static final HandlerList handlers = new HandlerList();
	 
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}
	private final int id;

	public CountDownFinish(int id) {
		this.id = id;
	}
	
	public int getID() {
		return id;
	}

}