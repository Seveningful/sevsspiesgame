
package be.seveningful.sevstaupegun.events;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.Team;

import be.seveningful.sevstaupegun.GameState;
import be.seveningful.sevstaupegun.Main;
import be.seveningful.sevstaupegun.managers.Taupe;
import be.seveningful.sevstaupegun.managers.TaupeManager;

public class NormalUHCEvents implements Listener {


	@EventHandler
	public void RespawTp(PlayerRespawnEvent e) {
		final Player p = e.getPlayer();
		new BukkitRunnable() {
			@Override
			public void run() {
				p.teleport(Main.instance.getConfiguration().getLobbyLocation());

				p.setGameMode(GameMode.SPECTATOR);
			}
		}.runTaskLater(Main.instance, 4L);
	}

	

	@EventHandler
	public void PlayerDeath(PlayerDeathEvent e) {
		Player player = e.getEntity();
		Main.instance.getGame().getAlivePlayers().remove(player.getUniqueId());

		 ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
		 SkullMeta meta = (SkullMeta) skull.getItemMeta();
		 meta.setOwner(player.getName());
		 skull.setItemMeta(meta);
		 e.getDrops().add(skull);
		 e.getDrops().add(new ItemStack(Material.GOLDEN_APPLE));
		 Team team = Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(player);
		 team.removePlayer(player);
		 e.setDeathMessage(team.getPrefix() + e.getEntity().getName()
				 + " est mort !");
		 for (Player pl : Bukkit.getOnlinePlayers()) {
			 pl.playSound(pl.getLocation(), Sound.WITHER_DEATH, 10.0F, 10.0F);
		 }

		 team.removeEntry(player.getName());
		 

		 if (TaupeManager.getTaupeByUUID(player.getUniqueId()) != null) {
			 Taupe taupe = TaupeManager.getTaupeByUUID(player.getUniqueId());
			 taupe.setDead(true);
		 }
		 if (TaupeManager.getSuperTaupeByUUID(player.getUniqueId()) != null) {
			 Taupe taupe = TaupeManager.getSuperTaupeByUUID(player.getUniqueId());
			 taupe.setDead(true);
		 }

	}

	@EventHandler
	public void cancelDamages(EntityDamageEvent e) {
		if(e.isCancelled()) return;
		if((Main.instance.getGame().getGameState() ==GameState.STARTING || Main.instance.getGame().getGameState() ==GameState.PREPARING ) && e.getEntity() instanceof Player)
			e.setCancelled(true);
		
	}

	@EventHandler
	public void regain(EntityRegainHealthEvent e) {

			if(e.getRegainReason() == RegainReason.SATIATED)
			e.setCancelled(true);


	}

	@EventHandler
	public void CancelPVp2(EntityDamageByEntityEvent e) {
		if ( (Main.instance.getGame().getGameState() ==GameState.STARTING || Main.instance.getGame().getGameState() == GameState.PREPARING ||Main.instance.getGame().getGameState() == GameState.NO_PVP) && ((e.getDamager() instanceof Player))
				&& ((e.getEntity() instanceof Player) && (e.getDamager() instanceof Player))) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void Food(FoodLevelChangeEvent e) {

		if(Main.instance.getGame().getGameState() == GameState.STARTING || Main.instance.getGame().getGameState() == GameState.PREPARING) {
			e.setCancelled(true);
		}

	}

	@EventHandler
	public void Teleport(PlayerMoveEvent e) {

		Player p = e.getPlayer();
		if (p.isOp()) return;
		if (!Main.instance.getGame().getAlivePlayers().contains(p.getUniqueId()) && Main.instance.getGame().getGameState() != GameState.STARTING ) {

			List<Entity> entitiesaround = p.getNearbyEntities(50,50, 50);

			List<Player> playersaround = new ArrayList<Player>();
			for (Entity nearby : entitiesaround) {

				if (nearby instanceof Player
						&& Main.instance.getGame().getAlivePlayers().contains(nearby.getUniqueId())) {
					playersaround.add((Player) nearby);

				}
			}

			if (playersaround.size() == 0) {
				int random = new Random().nextInt(Main.instance.getGame().getAlivePlayers().size());

				UUID p1 = Main.instance.getGame().getAlivePlayers().get(random);
				p.teleport(Bukkit.getPlayer(p1));
			}

		}
	}

	
	@EventHandler
	public void CountDown(CountDownFinish e) {
		if(e.getID() == 4) {
			
			World world = Main.instance.getConfiguration().getGameWorld();
			world.setTime(6000);
			world.setGameRuleValue("doDayLightCycle", "false");
			Bukkit.broadcastMessage(ChatColor.RED + "Le temps s'est arret� !");
			
		} else if(e.getID() == 5) {
			List<Integer> alreadyusedIDs = new ArrayList<Integer>();
			for(Taupe t : TaupeManager.getAllAliveTaupes()) {
				
				if(alreadyusedIDs.contains(t.getTTeamID())) continue;
				if(t.isDead()) continue;
				t.setSuper();
				
				alreadyusedIDs.add(t.getTTeamID());
				
			}
			
		}else if(e.getID() == 6) {
			for(Taupe t : TaupeManager.getAliveSuperTaupes()) {
				if(t.isDead()) continue;
				t.superreveal(false);
			}
			
		}
	}
	
	@EventHandler
	public void Damager(EntityDamageByEntityEvent e) {
		if(!(e.getDamager() instanceof Player)) return;
		Player damager = (Player) e.getDamager();
		if(damager.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
			
			e.setDamage(e.getDamage() / 2);
			
		}
	}
	
	@EventHandler
	public void Drop(PlayerDropItemEvent e) {
		if(GameState.STARTING == Main.instance.getGame().getGameState()) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void Damager(PlayerMoveEvent e) {
		if(Main.instance.getGame().getGameState() != GameState.STARTING && Main.instance.getGame().getAlivePlayers().contains(e.getPlayer().getUniqueId())) {
			Main.instance.getScoreboard().updateCentre(e.getPlayer());
		}
	}


}
