package be.seveningful.sevstaupegun.events;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;

import be.seveningful.sevstaupegun.GameState;
import be.seveningful.sevstaupegun.Main;
import be.seveningful.sevstaupegun.managers.Team;

public class ChoiceTeam implements Listener {


	@EventHandler
	public void Options(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action a = e.getAction();
		
		if (Main.instance.getGame().getGameState() == GameState.STARTING) {
			if ((a.equals(Action.RIGHT_CLICK_AIR))
					|| (a.equals(Action.RIGHT_CLICK_BLOCK))) {
				if (p.getItemInHand().getType() == Material.BANNER) {
					if (p.getItemInHand().getItemMeta().getDisplayName()
							.equals(ChatColor.GOLD + "Choisir son �quipe")) {
						e.setCancelled(true);

						openTeamInv(p);
					}
				} 
			}
		}
	}

	private void openTeamInv(Player p) {
		Inventory inv = Bukkit.createInventory(p, 9, ChatColor.GOLD
				+ "    Choisir son �quipe");
		for(Team team : Main.instance.getTeamRegister().getregisteredTeams()) {
			addItem(inv, team, team.getId());
		}
		ItemStack stack = new ItemStack(Material.BARRIER);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(ChatColor.RED + ChatColor.BOLD.toString() + "Random");
		stack.setItemMeta(meta);
		
		inv.setItem(8, stack);
		p.openInventory(inv);
	}


	@SuppressWarnings("deprecation")
	public static void addItem(Inventory inv, Team team, int slot) {
		ItemStack banner = new ItemStack(Material.BANNER);
		BannerMeta meta = (BannerMeta) banner.getItemMeta();
		meta.setDisplayName(team.getChatcolor() + team.getName() + team.getTeam().getSuffix());
		meta.setBaseColor(team.getDyecolor());
		List<String> lore = new ArrayList<String>();
		for (OfflinePlayer pl : team.getTeam().getPlayers()) {
			lore.add(team.getChatcolor() + "> " + pl.getName());
		}
		meta.setLore(lore);
		banner.setItemMeta(meta);
		inv.setItem(slot - 1 , banner);
	}


	/**
	 * Choice Team on Menu
	 */

	@EventHandler
	public void ChoiceTeamEvent(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (e.getInventory().getName()
				.equals(ChatColor.GOLD + "    Choisir son �quipe")) {
			if (e.getCurrentItem().getType() == Material.BANNER) {
				BannerMeta banner = (BannerMeta) e.getCurrentItem()
						.getItemMeta();
				for(Team team : Main.instance.getTeamRegister().getregisteredTeams()) {


					if ((banner.getBaseColor() == team.getDyecolor())
							) {
						if( team.getTeam().getPlayers().size() < Main.instance.getConfiguration().getPlayersPerTeam()) {
							p.sendMessage(team.getChatcolor() + "�"
									+ ChatColor.RESET
									+ " Vous avez rejoint l'�quipe " + team.getName());
							team.getTeam().addPlayer(p);
							p.setPlayerListName(team.getTeam().getPrefix() + p.getName() + ChatColor.RESET);
						} else {
							p.sendMessage(ChatColor.RED + "Cette �quipe est compl�te !");
						}
					}
				}
				p.setDisplayName(Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getPrefix()
						+ p.getName() + Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getSuffix());
				e.setCancelled(true);
				openTeamInv(p);
			} else if (e.getCurrentItem().getType() == Material.BARRIER) {
				if(Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p) != null)
				Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).removeEntry(e.getWhoClicked().getName());
				e.getWhoClicked().sendMessage(ChatColor.RED + "Vous avez quitt� votre �quipe !");
				((Player)e.getWhoClicked()).setDisplayName(e.getWhoClicked().getName());
				e.setCancelled(true);
				
			}
		}
	}
}
