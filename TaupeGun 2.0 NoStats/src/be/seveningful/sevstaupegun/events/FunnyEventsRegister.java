package be.seveningful.sevstaupegun.events;


import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.util.Vector;

import be.seveningful.sevstaupegun.GameState;
import be.seveningful.sevstaupegun.Main;

public class FunnyEventsRegister implements Listener {
	
	@EventHandler
	public void Dmaage(EntityDamageEvent e) {
		if(!(e.getEntity() instanceof Player))return;
		if(Main.instance.getGame().getGameState() != GameState.FINISHED) return;
		if(e.getCause() == DamageCause.FALL) e.setCancelled(true);
	}
	
	@EventHandler
	public void Touch(ProjectileHitEvent e) {
		if(!(e.getEntity() instanceof Snowball))return;
		if(Main.instance.getGame().getGameState() != GameState.FINISHED) return;
		Snowball ent  = (Snowball) e.getEntity();
		for (Entity entity : ent.getNearbyEntities(3, 3, 3)) {
			
			entity.setVelocity(new Vector(0, 20, 0));
		}
		ent.setBounce(false);
			
			
			
			if(ent instanceof Snowball) {
				
				int radius = 5;
				Location loc = ent.getLocation();
				
				    	  for(Location locs : sphere(ent.getLocation(), 3)) {
				    		  
				    		 locs.getBlock().setType(Material.WOOL);;
				    		 locs.getBlock().setData(getRandomColor());
				    	  
				    	  
				    	  
				    	  }
				     
				   }
				}
				
			
		
	
	public Set<Location> sphere(Location location, int radius){
        Set<Location> blocks = new HashSet<Location>();
        World world = location.getWorld();
        int X = location.getBlockX();
        int Y = location.getBlockY();
        int Z = location.getBlockZ();
        int radiusSquared = radius * radius;
 
      
            for (int x = X - radius; x <= X + radius; x++) {
                for (int y = Y - radius; y <= Y + radius; y++) {
                    for (int z = Z - radius; z <= Z + radius; z++) {
                        if ((X - x) * (X - x) + (Y - y) * (Y - y) + (Z - z) * (Z - z) <= radiusSquared) {
                            Location block = new Location(world, x, y, z);
                            if( block.getBlock().getType() != Material.AIR)
                            blocks.add(block);
                        }
                    }
                }
            }
            return blocks;
        
    }
	
	private byte getRandomColor() {
		Random r = new Random() ;
		
		int id = r.nextInt(10);
		if(id == 1){
			return DyeColor.PINK.getWoolData();
		} else if(id == 2) {
			return DyeColor.CYAN.getWoolData();
		} else if (id == 3) {
			return DyeColor.LIGHT_BLUE.getWoolData();
		} else if (id == 4) {
			return DyeColor.MAGENTA.getWoolData();
		} else {
			return DyeColor.ORANGE.getWoolData();
		}
	}
}
