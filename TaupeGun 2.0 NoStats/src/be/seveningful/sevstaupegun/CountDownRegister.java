package be.seveningful.sevstaupegun;

import java.util.ArrayList;
import java.util.List;

public class CountDownRegister {

	List<CountDown> countdowns = new ArrayList<CountDown>();
	
	public CountDownRegister() {
	
		
	
	}
	
	public void registerCountDown(int id, String name, int minutes) {
		
		
		countdowns.add(new CountDown(id,minutes,name));
		
	}
	
	
	
	public List<CountDown> getCountDowns() {
		return countdowns;
	}
}
