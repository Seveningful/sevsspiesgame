package be.seveningful.sevstaupegun;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;

import be.seveningful.chunkloader.MOTD;
import be.seveningful.sevstaupegun.managers.TaupeManager;
import be.seveningful.sevstaupegun.managers.Team;

public class Game {

	String version;
	
	List<UUID> alive = new ArrayList<UUID>();
	
	GameState state ;
	
	String name = "Taupe Gun";
	public Game(String version) {

	
	this.version = version;
	name = version;
	
	state = GameState.STARTING;
	countdown();
	}
	
	private void countdown() {
		
		 new BukkitRunnable() {
			 int seconds = 30;
			 
			 @Override
			public void run() {
				if(Main.instance.getGame().getGameState() != GameState.STARTING) {
					cancel();
					return;
				}
				setGameState(GameState.STARTING);
				if(!Main.instance.getConfiguration().hasCooldown())  cancel(); 
				if (Bukkit.getOnlinePlayers().size() >= Main.instance.getConfiguration().getMinimumPlayers()) {
					seconds--;
					if (seconds == 30 || seconds == 10 || (seconds <= 5 && seconds != 0)) {
						Methods.broadcastTime(seconds);
						
					}
					if(seconds == 0){
						
						Methods.setXpTime(seconds);
						start();
						cancel();
						
						
					}
				}else{
					seconds = 30;
				}
				Methods.setXpTime(seconds);

			}
		}.runTaskTimer(Main.instance, 0, 20L);
	}
	
	public void start() {
		setGameState(GameState.PREPARING);
		
		/*
		 * Join Random team
		 */
		Methods.fillTeams();
		Bukkit.getWorld("world").setTime(0);
		Bukkit.getWorld("world").setThundering(false);
		Bukkit.getWorld("world").setStorm(false);
		Bukkit.getWorld("world").setWeatherDuration(Integer.MAX_VALUE);
		
		for( Player p : Bukkit.getOnlinePlayers()) {
			//Main.instance.getScoreboard().setUpDefaultScoreBoard(p);
			p.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(Main.instance.getConfiguration().getGameName());
			p.getInventory().clear();
			alive.add(p.getUniqueId());
			p.setGameMode(GameMode.SURVIVAL);
			p.setLevel(0);
		}
		
		
		
		/*
		 * Teleportation
		 */
		ArrayList<Team> teams = (ArrayList<Team>) Main.instance.getTeamRegister().getregisteredTeams().clone();
		for(Team team : teams) {
			
		if(team.getTeam() == null ||team.getTeam().getPlayers().size() == 0) {
				
				team.unregister();
				continue;
			}
			
			for(OfflinePlayer player : team.getTeam().getPlayers()) {
				if(player.isOnline())
				((Player) player).teleport(team.getLoc());
				
			}
			
		}
		/*
		 * Taupes
		 */
		
		new TaupeManager(Main.instance);
		new GameLoop(Main.instance);
		
	}
	
	
	public GameState getGameState() {
		return state;
	}
	public void setGameState(GameState state) {
		this.state = state;
		MOTD.setMOTD(state.getMOTD());
	}
	
	public List<UUID> getAlivePlayers(){
		
		return alive;
		
	}

	public String getVersion() {
		return name;
	}
	

	
}
