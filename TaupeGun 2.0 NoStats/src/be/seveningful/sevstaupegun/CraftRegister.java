package be.seveningful.sevstaupegun;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public class CraftRegister {

	
	public CraftRegister() {

		ShapedRecipe craft = new ShapedRecipe(new ItemStack(
				Material.SPECKLED_MELON));
		craft.shape(new String[] { "***", "*x*", "***" });
		craft.setIngredient('*', Material.GOLD_INGOT);
		craft.setIngredient('x', Material.MELON);
		Bukkit.addRecipe(craft);

	
	}
}
