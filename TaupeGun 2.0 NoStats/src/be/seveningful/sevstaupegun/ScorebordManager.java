package be.seveningful.sevstaupegun;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World.Environment;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.json.simple.JSONObject;

import be.seveningful.sevstaupegun.events.JoinEvents;
import be.seveningful.sevstaupegun.managers.ScoreboardVariables;
import be.seveningful.sevstaupegun.managers.TitleManager;

import com.google.gson.Gson;

public class ScorebordManager {
	Gson gson;
	HashMap<ScoreboardVariables, String> values = new HashMap<ScoreboardVariables, String>();
	Map<UUID, Integer> centre = new HashMap<UUID, Integer>();
	Map<UUID, Scoreboard> scoreboards = new HashMap<UUID, Scoreboard>();
	ScorebordManager(int minutes) {


		
		gson = new Gson();
		
		values.put(ScoreboardVariables.BORDER, (int) -Main.instance.getConfiguration().getGameWorld().getWorldBorder().getSize()/2 + " | " + (int) Main.instance.getConfiguration().getGameWorld().getWorldBorder().getSize() /2);
		values.put(ScoreboardVariables.TIME, "20:00 " + ChatColor.RESET + ChatColor.BOLD.toString() + " |" + ChatColor.GRAY + " �pisode " + ChatColor.RESET + ChatColor.BOLD.toString() + "1");
		values.put(ScoreboardVariables.PLAYERS, ChatColor.BOLD + "0" + ChatColor.GRAY + " Joueurs");
		values.put(ScoreboardVariables.TEAMS,  ChatColor.BOLD + "0" + ChatColor.GRAY + " �quipes");
		//values.put(ScoreboardVariables.TAUPES_ALIVE,  ChatColor.BOLD + "0" + ChatColor.GRAY + " Taupes en vie");



	}


	public void updateToAll(int seconds, int minutes, int episode) {
		for(Player p : Bukkit.getOnlinePlayers()) {
			
			Scoreboard s = getScoreboard(p.getUniqueId());
			//System.out.println(gson.toJson(Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) + " and "+ gson.toJson(p.getScoreboard().getTeams()));
			if(!Bukkit.getScoreboardManager().getMainScoreboard().getTeams().containsAll(p.getScoreboard().getTeams())){
			for ( Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams() ) {
				Team newTeam = p.getScoreboard().getTeam(team.getName());
				if(s.getTeam(team.getName()) == null) {
				newTeam = s.registerNewTeam(team.getName());
				newTeam.setDisplayName(team.getDisplayName());
				newTeam.setPrefix(team.getPrefix());
				newTeam.setSuffix(team.getSuffix());
				for(OfflinePlayer players : team.getPlayers()) {
					newTeam.addPlayer(players);
				}
				} else  {
					newTeam = s.getTeam(team.getName());
					for(String players : newTeam.getEntries()) {
						if(!team.getEntries().contains(players))
						newTeam.removeEntry(players);
					}
					for(String players : team.getEntries()) {
						newTeam.addEntry(players);
					}
				}
			}
			for(String values: s.getEntries()) {
				if(!values.contains("Centre"))
				s.resetScores(values);
			}
			

		}
		}
		
		DecimalFormat df = new DecimalFormat("00");
		
		
		values.put(ScoreboardVariables.BORDER, (int) -Main.instance.getConfiguration().getGameWorld().getWorldBorder().getSize()/2 + " | " + (int) Main.instance.getConfiguration().getGameWorld().getWorldBorder().getSize() /2);
		values.put(ScoreboardVariables.TIME, df.format(minutes) + ":" + df.format(seconds) + " " + ChatColor.RESET + ChatColor.BOLD.toString() + " | " + ChatColor.GRAY + " �pisode " + ChatColor.RESET + ChatColor.BOLD.toString() + episode);
		values.put(ScoreboardVariables.PLAYERS, ChatColor.BOLD.toString() + Main.instance.getGame().getAlivePlayers().size() + ChatColor.GRAY + " Joueurs");
		values.put(ScoreboardVariables.TEAMS,  ChatColor.BOLD.toString() + Main.instance.getTeamRegister().getregisteredTeams().size() + ChatColor.GRAY + " �quipes");
		//values.put(ScoreboardVariables.TAUPES_ALIVE,  ChatColor.BOLD.toString() + TaupeManager.getAliveTaupes().size() + ChatColor.GRAY + " Taupes en vie");
		
		
		
		
		for(final Player p : Bukkit.getOnlinePlayers()) {

			/*
			 * Tab
			 * Time
			 */
			
			String footer  =  "\n";
			for(CountDown countdown : Main.instance.countdownregister.getCountDowns()) {
				if(countdown.finished) continue;
				footer = footer + countdown.getName() + " " + ChatColor.RESET + ChatColor.BOLD.toString()  + ChatColor.YELLOW + df.format(countdown.getMinutes()) + ":" + df.format(countdown.getSeconds() ) + "\n";
			}
			TitleManager.setPlayerList(p, Main.instance.game.getVersion(), footer);
			
			Scoreboard s = p.getScoreboard();
	
			Objective obj = s.getObjective(Main.instance.getGame().getVersion());
			if(obj == null) obj = s.registerNewObjective(Main.instance.getGame().getVersion(), "dummy");
			if(obj.getDisplaySlot() != DisplaySlot.SIDEBAR) obj.setDisplaySlot(DisplaySlot.SIDEBAR);
			for(String values: s.getEntries()) {
				if(!values.contains("Centre"))
				s.resetScores(values);
			}
			
			new BukkitRunnable() {
				
				@Override
				public void run() {
					Objective obj = p.getScoreboard().getObjective("healthTG");
					if(obj == null) obj = p.getScoreboard().registerNewObjective("healthTG", "health");
					if(obj.getDisplaySlot() != DisplaySlot.SIDEBAR) obj.setDisplaySlot(DisplaySlot.PLAYER_LIST);
				}
			}.runTaskLater(Main.instance, 20*1);
			/*
			 * Players 
			 */

			obj.getScore(values.get(ScoreboardVariables.PLAYERS)).setScore(-2);
			//obj.getScore(values.get(ScoreboardVariables.TAUPES_ALIVE)).setScore(-1);

			/*
			 * Teams
			 */
			obj.getScore(" ").setScore(-2);
			obj.getScore(values.get(ScoreboardVariables.TEAMS)).setScore(-3);
			obj.getScore("  ").setScore(-4);

			/*
			 * Borders
			 */
			obj.getScore(ChatColor.GOLD + ChatColor.BOLD.toString() + "Bordures").setScore(-5);
			obj.getScore(values.get(ScoreboardVariables.BORDER)).setScore(-6);

			/*
			 * Time
			 */
			obj.getScore("   ").setScore(-7);
			obj.getScore(values.get(ScoreboardVariables.TIME)).setScore(-8);


		}


	}

	public void updateCentre(Player p) {
		for(String s : getScoreboard(p.getUniqueId()).getEntries()) {
			if(s.contains("Centre")) {
				getScoreboard(p.getUniqueId()).resetScores(s);
			}
		}
		getScoreboard(p.getUniqueId()).resetScores(ChatColor.BOLD.toString()  + "Centre " + ChatColor.GRAY + centre.get(p.getUniqueId()));
		if(p.getWorld().getEnvironment() != Environment.NORMAL) {
			
			getScoreboard(p.getUniqueId()).getObjective(DisplaySlot.SIDEBAR).getScore(ChatColor.BOLD.toString()  + "Centre " + ChatColor.RED + "Incalcuable").setScore(-4);
			return;
		}
		int y = Main.instance.getConfiguration().getGameWorld().getHighestBlockYAt(0, 0);
		centre.put(p.getUniqueId(),(int) new Location(Main.instance.getConfiguration().getGameWorld(), 0 , y , 0).distance(p.getLocation()));
		getScoreboard(p.getUniqueId()).getObjective(DisplaySlot.SIDEBAR).getScore(ChatColor.BOLD.toString()  + "Centre " + ChatColor.GRAY + centre.get(p.getUniqueId())).setScore(-4);
	}
	

	public void setUpDefaultScoreBoard(Player p) {
		Scoreboard s = getScoreboard(p.getUniqueId());
		p.setScoreboard(s);

		Objective obj = s.registerNewObjective(Main.instance.getGame().getVersion(), "dummy");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		Objective obj2 = s.registerNewObjective("health", "health");
		obj2.setDisplaySlot(DisplaySlot.PLAYER_LIST);
		/*
		 * Players 
		 */
		for(String values: s.getEntries()) {
			if(!values.contains("Centre"))
			s.resetScores(values);
		}
		obj.getScore(values.get(ScoreboardVariables.PLAYERS)).setScore(-2);
		//obj.getScore(values.get(ScoreboardVariables.TAUPES_ALIVE)).setScore(-1);

		/*
		 * Teams
		 */
		obj.getScore(" ").setScore(-2);
		obj.getScore(values.get(ScoreboardVariables.TEAMS)).setScore(-3);
		
		/*
		 * Center
		 */
		obj.getScore(ChatColor.BOLD.toString()  + "Centre " + ChatColor.GRAY + "0").setScore(-4);
		centre.put(p.getUniqueId(), 0);
		
		
		/*
		 * Borders
		 */
		obj.getScore(ChatColor.GOLD + ChatColor.BOLD.toString() + "Bordures").setScore(-5);
		obj.getScore(values.get(ScoreboardVariables.BORDER)).setScore(-6);

		/*
		 * Time
		 */
		obj.getScore("   ").setScore(-7);
		obj.getScore(values.get(ScoreboardVariables.TIME)).setScore(-8);
		JoinEvents.scoreboards.put(p.getUniqueId(), s);
	}
	
	public Scoreboard getScoreboard(UUID id) {
		if(!scoreboards.containsKey(id)) {
			scoreboards.put(id, Bukkit.getScoreboardManager().getNewScoreboard());
		}
		return scoreboards.get(id);
	}

}
