package be.seveningful.sevstaupegun;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class HelpMessage {
	
	static Map<String, String> commandslist = new HashMap<String, String>();

	
	public static void registerCommand(String command, String usage) {
		commandslist.put(command, usage);
	}
	
	
	public static void sendHelpMessage(Player p) {
		String header = ChatColor.GRAY + ChatColor.BOLD.toString() + "-------[" 
						+ ChatColor.YELLOW + ChatColor.BOLD.toString() + "SSG" 
						+ ChatColor.GRAY + ChatColor.BOLD.toString() + "]-------";
		String footer = ChatColor.GRAY + ChatColor.BOLD.toString() + "-----------------";
		p.sendMessage(header);
		for(String command : commandslist.keySet()) {
			String usage = commandslist.get(command);
			
			String messagehelp = ChatColor.YELLOW + command + " " + ChatColor.GRAY + "- " + usage;
			p.sendMessage(messagehelp);
		}
		
		p.sendMessage(footer);
	}
}
