package be.seveningful.sevstaupegun;

import org.bukkit.ChatColor;

public enum GameState {
	STARTING(ChatColor.translateAlternateColorCodes('&', Main.instance.getConfiguration().getMOTD("starting"))),
	RUNNING(ChatColor.translateAlternateColorCodes('&', Main.instance.getConfiguration().getMOTD("running"))),
	FINISHED(ChatColor.translateAlternateColorCodes('&', Main.instance.getConfiguration().getMOTD("finished"))), 
    PREPARING(ChatColor.translateAlternateColorCodes('&', Main.instance.getConfiguration().getMOTD("prepare"))), 
    		NO_PVP(ChatColor.translateAlternateColorCodes('&', Main.instance.getConfiguration().getMOTD("no-pvp")));

	String motd;
	private GameState(String motd) {
		this.motd = motd;
	}
	public String getMOTD() {
		return motd;
	}
}
