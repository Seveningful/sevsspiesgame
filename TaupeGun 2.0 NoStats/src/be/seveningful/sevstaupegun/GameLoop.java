package be.seveningful.sevstaupegun;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldBorder;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import be.seveningful.sevstaupegun.events.CountDownFinish;
import be.seveningful.sevstaupegun.managers.TaupeManager;
import be.seveningful.sevstaupegun.managers.Team;
import be.seveningful.sevstaupegun.managers.TitleManager;

public class GameLoop {

	public GameLoop(final Main main) {
		
		/*
		 * Loop
		 */
		new BukkitRunnable() {
			
			@Override
			public void run() {
				List<Team> unregistred = new ArrayList<Team>();
				for(Team t : main.getTeamRegister().getregisteredTeams()) {
					if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam(t.getName()).getSize() == 0 ) {
						if(t.getName().contains("taupes")) {
							if(t.getName().contains("super")) {
								
								if(TaupeManager.getAliveSuperTaupes().size() != 0) continue;
								
							}else {
								if(TaupeManager.getAliveTaupes(Integer.valueOf(t.getName().replace("taupes", ""))).size() != 0) continue;
							}
						}
						Bukkit.broadcastMessage("L'�quipe " +t.getTeam().getPrefix() + t.getTeam().getName() + t.getTeam().getSuffix() + " a �t� �limin�e");
						unregistred.add(t);
					}
				}
				for(Team t : unregistred) t.unregister();
			}
		}.runTaskTimer(Main.instance, 0, 1);
		
		/*
		 * Scoreboard
		 */
		new BukkitRunnable() {
			int episode = 1;
			int seconds = 0;
			int minutes = 20;

			@Override
			public void run() {
				/*
				 * Team checker
				 */

				for(CountDown countdown : main.countdownregister.getCountDowns()) {
					if (countdown.finished()) continue;
					if (countdown.getSeconds() == 0) {
						if (countdown.getMinutes() == 0) {



							main.getServer().getPluginManager().callEvent(new CountDownFinish(countdown.id));
							countdown.finished = true;

						} else {
							countdown.setSeconds(59); 
							countdown.setMinutes(countdown.getMinutes() -1 ); 

						}
						
					} else {
						
						countdown.setSeconds(countdown.getSeconds() -1);
					}
					
				}

				if (seconds == 0) {
					if (minutes == 0) {


						episode += 1;
						Bukkit.broadcastMessage(ChatColor.AQUA
								+ "------------- Episode " + episode
								+ " -------------");

						seconds = 59;
						minutes = 19;

					} else {
						seconds = 59;
						minutes -= 1;

					}
				} else {
					seconds -= 1;
				}

				main.getScoreboard().updateToAll(seconds, minutes,episode);

				if(Bukkit.getScoreboardManager().getMainScoreboard().getTeams().size() == 1) {
					//Bukkit.getScheduler().cancelAllTasks();
					org.bukkit.scoreboard.Team winner = null;
					Player teleport = null;
					for(org.bukkit.scoreboard.Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
						winner = team;
					}
					
					for(OfflinePlayer winners : winner.getPlayers()) {
						
						if(winners.isOnline()) {
							
							while(winners.getPlayer().getInventory().firstEmpty() != winners.getPlayer().getInventory().getSize() -1) {
								winners.getPlayer().getInventory().addItem(new ItemStack(Material.SNOW_BALL, 16));
							}
							ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS);
							boots.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 1000000);
							winners.getPlayer().getInventory().setBoots(boots);
							teleport = winners.getPlayer();
						}
						
					}
					Bukkit.broadcastMessage("L'�quipe " + winner.getDisplayName() + ChatColor.RESET + " a gagn� !");
					main.game.setGameState(GameState.FINISHED);
					for(Player p : Bukkit.getOnlinePlayers() ){
						
						TitleManager.sendSubTitle("L'�quipe " + winner + ChatColor.RESET + " a gagn� !", p, 10, 200, 10);
						p.teleport(teleport);
						p.setGameMode(GameMode.SURVIVAL);
					}

					cancel();
					return;
				}
			}
		}.runTaskTimer(main, 0, 20);


		/*
		 * WorldBorder
		 */
		new BukkitRunnable() {
			@Override
			public void run() {
				World w = main.getConfiguration().getGameWorld();
				WorldBorder wb = w.getWorldBorder();

				Bukkit.broadcastMessage(ChatColor.RED + "Les bordures se r�tr�cissent ! Rapprochez vous du centre !");
				wb.setSize(main.getConfiguration().getWBFinalSize(), 60*main.getConfiguration().getRetractTime());
				
				for(UUID p : Main.instance.game.alive){
					
					Player player = Bukkit.getPlayer(p);
					
					if(player.getWorld().getEnvironment() == Environment.NETHER) {
						player.teleport(Main.instance.teamsregister.getTeamOfPlayer(player).getLoc());
						player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20*20, 20, false, false));
					}
				}
			}
		}.runTaskLater(main, 20*60*main.getConfiguration().getWBRetractAfterMinutes());

		/*
		 * PVP
		 */
		new BukkitRunnable() {
			@Override
			public void run() {
				main.getGame().setGameState(GameState.RUNNING);
				Bukkit.broadcastMessage(ChatColor.RED + "Le PVP est maintenant actif !");
				for(Player p : Bukkit.getOnlinePlayers()) {

					TitleManager.sendActionBar(p, ChatColor.RED + ChatColor.BOLD.toString() + "Le PVP est maintenant actif !");

				}
			}
		}.runTaskLater(main,  20*60*main.getConfiguration().getPVPTime());


		/*
		 * DamageTime
		 */
		new BukkitRunnable() {
			@Override
			public void run() {
				main.getGame().setGameState(GameState.NO_PVP);
				Bukkit.broadcastMessage(ChatColor.RED + "Les d�gats sont maintenants actifs !");
				for(Player p : Bukkit.getOnlinePlayers()) {

					TitleManager.sendActionBar(p, ChatColor.RED + ChatColor.BOLD.toString() + "Les d�gats sont maintenant actifs !");

				}

			}
		}.runTaskLater(main, 20*main.getConfiguration().getNoDamageTime());

	}

}
