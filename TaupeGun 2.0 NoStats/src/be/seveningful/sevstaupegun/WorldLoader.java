package be.seveningful.sevstaupegun;

import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.WorldCreator;

import be.seveningful.chunkloader.ChunkLoader;

public class WorldLoader {



	public WorldLoader(Main main, boolean loadchunks) {

		main.getServer().createWorld(
				new WorldCreator(main.getConfiguration().getLobbyWorldName()));
		World w = main.getConfiguration().getGameWorld();

		WorldBorder wb = w.getWorldBorder();

		wb.setCenter(0, 0);
		wb.setDamageAmount(1);
		wb.setSize(main.getConfiguration().getWBSize());

		if(loadchunks)
			new ChunkLoader(main.getConfiguration().getGameWorld(), main);
	}
}
