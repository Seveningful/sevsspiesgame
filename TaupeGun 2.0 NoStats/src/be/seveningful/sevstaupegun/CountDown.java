package be.seveningful.sevstaupegun;

public class CountDown {

	int id , minutes, seconds;
	boolean finished = false;
	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	String name;

	public CountDown(int id, int minutes, String name) {
		this.id = id;
		this.minutes = minutes;
		this.name = name;
	}

	public boolean finished() {
		return finished;
	}

	public String getName() {
		return name;
	}






}
