package be.seveningful.sevstaupegun;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import be.seveningful.configmenu.OptionsMenu;
import be.seveningful.sevstaupegun.events.EventsRegister;
import be.seveningful.sevstaupegun.managers.ConfigManager;
import be.seveningful.sevstaupegun.managers.Taupe;
import be.seveningful.sevstaupegun.managers.TaupeManager;
import be.seveningful.sevstaupegun.managers.TeamsRegister;

public class Main extends JavaPlugin{

	/**
	 * Code for a private "TaupeGun / SpiesGame" game
	 * @author Sevening
	 * � 2015 Seveningful All Rights Reserved
	 */


	ConfigManager config;
	public static Main instance;
	TeamsRegister teamsregister;
	CountDownRegister countdownregister;
	Game game;
	ScorebordManager scoreboard;
	@Override
	public void onEnable() {
		System.out.println("+------------SevsSpiesGame v2----------+");
		System.out.println("|      Plugin cr�� par Seveningful     |");
		System.out.println("+--------------------------------------+");
		instance = this;
		config = new ConfigManager();


		config.registerDefaults(this);
		getConfig().options().copyDefaults(true);
		saveConfig();

		new EventsRegister(this);
		new CraftRegister();
		new WorldLoader(this, true);
		new OptionsMenu();
		new WorldSetup(this);
		teamsregister = new TeamsRegister();
		setupConfig();
		super.onEnable();
	}



	public void setupConfig() {
		instance = this;





		World w = config.getGameWorld();
		WorldBorder wb = w.getWorldBorder();
		wb.setSize(config.getWBSize());

		countdownregister = new CountDownRegister();
		countdownregister.registerCountDown(1, ChatColor.RED + "D�finition des Taupes", getConfiguration().getTimeToSetTaupes());
		countdownregister.registerCountDown(3, ChatColor.RED + "Auto-Reveal", getConfiguration().getForceRevealMinutes());

		if(config.hasSuperTaupes()) {
			countdownregister.registerCountDown(5, ChatColor.DARK_RED + "D�finition des SuperTaupes ", (int) getConfiguration().getSuperTaupesTime());
			countdownregister.registerCountDown(6, ChatColor.DARK_RED + "Auto-Reveal des SuperTaupes ", getConfiguration().getTimetoForceSuperReveal());
		}

		if(config.hasTimeCycle())
			countdownregister.registerCountDown(4, ChatColor.GREEN + "Eternal-Day  ", (int) getConfiguration().getCycleAfter());

		countdownregister.registerCountDown(2, ChatColor.GREEN + "R�traction des Bordures", getConfiguration().getWBRetractAfterMinutes());
		game = new Game(config.getGameName());


		HelpMessage.registerCommand("/sevsspiesgame", "Affiche l'aide du plugin");
		HelpMessage.registerCommand("/config", "Affiche le menu de modification d'options");
		HelpMessage.registerCommand("/start", "D�bute la partie");
		HelpMessage.registerCommand("/t - /st", "Pour parler entre Taupes/SuperTaupes");
		HelpMessage.registerCommand("/reveal - /sreveal", "Pour se r�v�ler en tant que Taupe/SuperTaupe");
		HelpMessage.registerCommand("/revive <name> <teamname>", "Faire revivre un joueur");
		/*
		 * World setup
		 */
		new WorldSetup(this);
		scoreboard = new ScorebordManager(20);

	}





	public ConfigManager getConfiguration() {
		return config;
	}


	public Game getGame() {
		return game;
	}



	public TeamsRegister getTeamRegister(){
		return teamsregister;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {

		if ( sender instanceof Player) {

			Player p = (Player) sender;
			if (label.equalsIgnoreCase("config")) {

				if (args.length == 0) {
					if(!p.isOp()) {
						p.sendMessage(ChatColor.DARK_RED + "Vous n'avez pas la permission de changer la configuration du serveur !");
						return true;
					}else if (getGame().getGameState() != GameState.STARTING) {

						p.sendMessage(ChatColor.DARK_RED + "La partie a d�j� commenc� !");
						return true;

					}
					if(OptionsMenu.getInstance() != null && OptionsMenu.getInstance().getViewers().size() == 1) {
						p.sendMessage(ChatColor.RED + "La configuration est d�j� entrain d'�tre chang�e par " + ChatColor.YELLOW + OptionsMenu.getInstance().getViewers().get(0).getName());
						return true;
					}
					OptionsMenu.openTo(p);
					return true;
				}
			} else
				if (label.equalsIgnoreCase("t")) {
					Taupe taupe = TaupeManager.getTaupeByUUID(p.getUniqueId());
					if( taupe != null && !taupe.isSuperRevealed()) {

						if(args.length == 0) return true;
						if(taupe.isDead()) return true;
						String message = StringUtils.join(args, ' ', 0,
								args.length);
						for(Taupe t : TaupeManager.getTaupes()) {

							if(Bukkit.getPlayer(t.getUuid()) == null) continue;
							if(t.getTTeamID() == taupe.getTTeamID() && !t.isSuperRevealed()) {
								Bukkit.getPlayer(t.getUuid()).sendMessage(
										ChatColor.GOLD + "(Taupes)" + ChatColor.RED
										+ "<" + Bukkit.getPlayer(taupe.getUuid()).getName() + "> "  + ChatColor.WHITE + message);
							}
						}
					} else{

						p.sendMessage(ChatColor.RED + "Vous n'�tes pas une taupe !");
					}

				}else
					if (label.equalsIgnoreCase("st")) {
						Taupe taupe = TaupeManager.getSuperTaupeByUUID(p.getUniqueId());
						if( taupe != null) {

							if(args.length == 0) return true;
							if(taupe.isDead()) return true;
							String message = StringUtils.join(args, ' ', 0,
									args.length);
							for(Taupe t : TaupeManager.getTaupes()) {
								if(!t.isSuper()) continue;
								if(Bukkit.getPlayer(t.getUuid()) == null) continue;
								if(t.getTTeamID() == taupe.getTTeamID()) {
									Bukkit.getPlayer(t.getUuid()).sendMessage(
											ChatColor.GOLD + "(Super Taupes)" + ChatColor.RED
											+ "<" + Bukkit.getPlayer(taupe.getUuid()).getName() + "> "  + ChatColor.WHITE + message);
								}
							}
						} else{

							p.sendMessage(ChatColor.RED + "Vous n'�tes pas une super taupe !");
						}

					}  else
						if (label.equalsIgnoreCase("sreveal")) {
							Taupe taupe = TaupeManager.getSuperTaupeByUUID(p.getUniqueId());
							if( taupe != null) {

								if(!taupe.superreveal(true)) {
									p.sendMessage(ChatColor.RED + "Vous vous �tes d�j� r�v�l� !");
								}

							} else{

								p.sendMessage(ChatColor.RED + "Vous n'�tes pas une taupe !");
							}

						} else
							if (label.equalsIgnoreCase("reveal")) {
								Taupe taupe = TaupeManager.getTaupeByUUID(p.getUniqueId());
								if( taupe != null) {

									if(!taupe.reveal(true)) {
										p.sendMessage(ChatColor.RED + "Vous vous �tes d�j� r�v�l� !");
									}

								} else{

									p.sendMessage(ChatColor.RED + "Vous n'�tes pas une taupe !");
								}

							} else
								if (label.equalsIgnoreCase("start")) {
									if (p.isOp()) {
										if(game.getGameState() == GameState.STARTING)
											Main.instance.getGame().start();

									} else {
										p.sendMessage(ChatColor.RED + "Vous n'avez pas la permission d'utiliser cette commande");
									}
								} 

								else
									if (label.equalsIgnoreCase("revive")) {
										if (!p.isOp()) {
											p.sendMessage(ChatColor.RED + "Vous n'avez pas la permission d'utiliser cette commande");
											return true;
										} 
										Player reviver = Bukkit.getPlayer(args[0]);
										if(reviver == null) {
											p.sendMessage(ChatColor.RED + "Ce joueur n'est pas en ligne !");
											return true;
										}
										game.alive.add(reviver.getUniqueId());
										reviver.teleport(p);
										reviver.setGameMode(GameMode.SURVIVAL);
										reviver.setFoodLevel(40);
										Bukkit.getScoreboardManager().getMainScoreboard().getTeam(args[1]).addEntry(reviver.getName());
										p.sendMessage(ChatColor.GREEN + "Ce joueur a �t� r�cussit� !");
										p.setDisplayName(Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getPrefix()
												+ p.getName() + Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getSuffix());
										p.setPlayerListName(Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getPrefix() + p.getName() + Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p).getSuffix());


									} else 
										if (label.equalsIgnoreCase("sevsspiesgame") || label.equalsIgnoreCase("ssg")) {
											HelpMessage.sendHelpMessage(p);

										}

		}
		return true;
	}

	static boolean isInteger(String junk){
		try{
			int x = Integer.parseInt(junk);
			return true;
		}
		catch(NumberFormatException e){
			return false;
		}
	}

	public ScorebordManager getScoreboard() {
		return scoreboard;
	}



	public void setTeamRegister(TeamsRegister teamsRegister2) {
		teamsregister = teamsRegister2;

	}




}
