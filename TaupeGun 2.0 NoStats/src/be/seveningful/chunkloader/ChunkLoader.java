package be.seveningful.chunkloader;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

import be.seveningful.sevstaupegun.Main;

public class ChunkLoader {
	public ChunkLoader(final World world,final  Main main) {
				boolean debug = false;
				double chunks = (main.getConfiguration().getWBSize() * main.getConfiguration().getWBSize())/256;
				double loaded = 0;
				System.out.println("[ChunkLoader] Counted " + chunks + " Chunks !");
				System.out.println("[ChunkLoader] Chargement des chunks en cours, cette opération peut prendre quelques secondes voire minutes variant selon la puissance de votre serveur et de la taille de la map ....");
				for (int x = (int) -(main.getConfiguration().getWBSize() / 2); x < (main.getConfiguration().getWBSize() / 2); x+=16) {
					for (int z = (int) -(main.getConfiguration().getWBSize() / 2); z < (main.getConfiguration().getWBSize() / 2); z+=16) {
						Chunk c = world.getChunkAt(new Location(world, x, 0, z));
						if (c.load()) {
							loaded++;
							if(debug)
								System.out.println("[ChunkLoader] Loaded " + c.toString()
										+ " located at " + x + ":" + z);
							double percent = loaded / chunks * 100;
							MOTD.setMOTD(ChatColor.DARK_PURPLE + "Génération de la map : "
									+ (int) percent + "%");

						} else {
							System.out
							.println("[ChunkLoader] An error occured while loading "
									+ c.toString()
									+ " located at "
									+ x
									+ ":"
									+ z + " , maybe it was already loaded ?");
						}
					}
				}

	}

}
