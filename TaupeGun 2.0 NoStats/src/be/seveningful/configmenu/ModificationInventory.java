package be.seveningful.configmenu;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftInventoryCustom;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import be.seveningful.sevstaupegun.Main;

public class ModificationInventory {

	public ModificationInventory(String optionname, Player p) {
		 Inventory inv = Bukkit.createInventory(null, InventoryType.ANVIL, "Modification d'option");
		
		ItemStack stack = new ItemStack(Material.NAME_TAG);
		ItemMeta meta = stack.getItemMeta();
		List<String> list = new ArrayList<String>();
		list.add(optionname);
		meta.setLore(list);
		meta.setDisplayName(ChatColor.BLUE + Main.instance.getConfig().getString(optionname));
		stack.setItemMeta(meta);
		
		inv.addItem(stack);
		p.openInventory(inv);
		System.out.println(true);
		
	}
	
	




}
