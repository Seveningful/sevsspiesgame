package be.seveningful.configmenu;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftInventoryCustom;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class OptionsMenu extends CraftInventoryCustom {
	public OptionsMenu() {
		super(null,9*6,ChatColor.GREEN + "Menu des options");
		update();
	}
	static OptionsMenu menu;
	static List<Option> options = new ArrayList<Option>();
	
	public static void registerOption(String displayName, String configname) {
		options.add(new Option(displayName, configname));
		
	}
	
	public void update() {
		clear();
		/*
		 * BLUE DYE = TEXT
		 * 
		 * ROSE/GREEN = BOOLEAN VALUE
		 * 
		 * YELLOW = NUMBER
		 * 
		 */
			for(Option option : options)
			{
				ItemStack stack =  new ItemStack(Material.INK_SACK, 1, (byte) 11);
				ItemMeta meta = stack.getItemMeta();
				List<String> list = new ArrayList<String>();
				list.add( option.getOptionname());
				meta.setLore(list);
				
				if(option.isInt) {
					 stack = new ItemStack(Material.INK_SACK, 1, (byte) 11);
					 meta.setDisplayName(ChatColor.GREEN + option.displayname + " " + ChatColor.YELLOW + option.currentintvalue );
				} else if (option.isBoolean) {
					 stack = new ItemStack(Material.INK_SACK, 1, option.currentBooleanValue ? (byte) 10 : (byte) 5);
					 meta.setDisplayName((option.currentBooleanValue ? ChatColor.GREEN : ChatColor.LIGHT_PURPLE) + option.displayname);
				} else {
					 stack = new ItemStack(Material.INK_SACK, 1, (byte) 4);
					 meta.setDisplayName(ChatColor.GREEN + option.displayname + ChatColor.BLUE + "(Changer le texte)" );
				}
				
				stack.setItemMeta(meta);
				
				
				addItem(stack);
				
				
				
			}
			
			ItemStack reload = new ItemStack(Material.SLIME_BALL);
			ItemMeta rmeta = reload.getItemMeta();
			rmeta.setDisplayName(ChatColor.GREEN + "Recharger la configuration");
			reload.setItemMeta(rmeta);
			addItem(reload);
	}

	
	public static void openTo(Player p) {
		
		if(menu == null) menu = new OptionsMenu();
		p.openInventory(menu);
		
	}
	public static OptionsMenu getInstance() {return menu;}
	
	
	public static Option getOption(String name) {
		
		for(Option option : options)
		{
			if(option.getOptionname().equalsIgnoreCase(name))
			return option;
		}
		return null;
	}

	
	
	
}
