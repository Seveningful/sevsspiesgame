package be.seveningful.configmenu;

import be.seveningful.sevstaupegun.Main;

public class Option {

	String displayname;
	String optionname;
	String currentvalue;
	boolean isInt;
	public boolean isBoolean;
	int maxvalue = 100;
	int minvalue = 0;
	
	int currentintvalue;
	public boolean currentBooleanValue;
	public Option(String displayname, String optionname,
			int maxvalue, int minvalue, int currentvalue) {
		this.displayname = displayname;
		this.optionname = optionname;
		this.isInt = true;
		this.maxvalue = maxvalue;
		this.minvalue = minvalue;
		this.currentintvalue = currentvalue;
	}
	
	public Option(String displayname, String optionname) {
		this.displayname = displayname;
		this.optionname = optionname;
		
		if ("true".equals(Main.instance.getConfig().getString(optionname)) || "false".equals(Main.instance.getConfig().getString(optionname))) {
			isBoolean = true;
			  currentBooleanValue = Boolean.parseBoolean(Main.instance.getConfig().getString(optionname));
			} else {
				try {
					System.out.println(Integer.parseInt(Main.instance.getConfig().getString(optionname)));;
					isInt = true;
					currentintvalue = Main.instance.getConfig().getInt(optionname);
				} catch (Exception e2) {
				}
			}
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getOptionname() {
		return optionname;
	}

	public void setOptionname(String optionname) {
		this.optionname = optionname;
	}

	public boolean isInt() {
		return isInt;
	}

	public void setInt(boolean isInt) {
		this.isInt = isInt;
	}

	public int getMaxvalue() {
		return maxvalue;
	}

	public void setMaxvalue(int maxvalue) {
		this.maxvalue = maxvalue;
	}

	public int getMinvalue() {
		return minvalue;
	}

	public void setMinvalue(int minvalue) {
		this.minvalue = minvalue;
	}

	public int getCurrentvalue() {
		return currentintvalue;
	}

	public void setCurrentvalue(String currentvalue) {
		this.currentvalue = currentvalue;
		Main.instance.getConfig().set(getOptionname(), currentvalue);
	}

	public void setCurrentBooleanValue(boolean value) {
		this.currentBooleanValue = value;
		Main.instance.getConfig().set(getOptionname(), value);
	}

	public void setCurrentIntvalue(int parseInt) {
		this.currentintvalue = parseInt;
		Main.instance.getConfig().set(getOptionname(), parseInt);
		
	}

	public int getCurrentIntValue() {
		return currentintvalue;
	}

	
	
}