package be.seveningful.configmenu;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.server.v1_8_R3.ChatMessage;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.PacketPlayOutOpenWindow;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import be.seveningful.sevstaupegun.Main;

public class Anvil {

	public static void openAnvilInventory(final Player player, String optionname) {

		EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();

		FakeAnvil fakeAnvil = new FakeAnvil(entityPlayer);

		int containerId = entityPlayer.nextContainerCounter();

		((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutOpenWindow(containerId, "minecraft:anvil", new ChatMessage("Repairing", new Object[]{}), 0));


		entityPlayer.activeContainer = fakeAnvil;


		entityPlayer.activeContainer.windowId = containerId;

		entityPlayer.activeContainer.addSlotListener(entityPlayer);

		entityPlayer.activeContainer = fakeAnvil;
		entityPlayer.activeContainer.windowId = containerId;
		Inventory inv = fakeAnvil.getBukkitView().getTopInventory();

		ItemStack stack = new ItemStack(Material.NAME_TAG);
		ItemMeta meta = stack.getItemMeta();
		List<String> list = new ArrayList<String>();
		list.add(optionname);
		meta.setLore(list);
		meta.setDisplayName(Main.instance.getConfig().getString(optionname));
		stack.setItemMeta(meta);
		inv.addItem(stack);


	}


}
